const fs = require('fs');
const dotenv = require('dotenv');
const mongoose = require("mongoose");
const Tour = require('../../models/tourModel');

dotenv.config({ path: './config.env' });

const DB = process.env.DATABASE;
console.log(DB);
// return
mongoose.connect(DB).then((con) => console.log(con.connection + 'DB connection successful!')).catch(err => console.log('something went wrong', err))

const tours = JSON.parse(fs.readFileSync(`${__dirname}/tours-simple.json`, 'utf-8'));
// console.log(tours)
const importData = async () => {
  console.log('imp')
  try {
    await Tour.create(tours)
  }catch(e) {
    console.log(e)
  }
};

const deleteData = async() => {
  console.log('delete')

  try {
    await Tour.deleteMany();
    console.log('Data successfully deleted')
  } catch (error) {
    console.log(error)
  }
};

/**
 * if you want to reupload comment out this items
 */
// importData();
// deleteData();
console.log(process.argv)