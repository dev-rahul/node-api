const Tour = require("./../models/tourModel");

exports.getAllTours = async(req, res) => {
  try{
    /**
     * Simple filtering
     * 1A
     */
    const queryObj = {...req.query};
    const excludeFields = ['page', 'sort', 'limit', 'fields'];
    excludeFields.forEach(el => delete queryObj[el]);

    /**
     * 
     * 1A
     * Advance filtering
     * 
     * Pass something like this to get advance results data `duration[lt]=5`
     */
    let queryStr = JSON.stringify(queryObj);
    queryStr = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, match => `$${match}`);

    let query = Tour.find(JSON.parse(queryStr));
    if(req.query.sort) {
      const sortBy = req.query.sort.replaceAll(',', ' ')
      query = query.sort(sortBy)
    }else {
      query = query.sort('-createdAt')
    }

    /**
     * execute query
     */
    const tours = await query;
    
    res.status(200).json({
      status: 'success',
      requestedAt: req.requestTime,
      result: tours.length,
      data: {
        tours
      }
    });
  }catch(e) {
    res.status(400).json({
      status: 'faai',
      message: e.message
    });
  }
};

exports.getTour = async(req, res) => {
  try {
    /**
     * if you want to get item by id
     */
    const tour = await Tour.findById(req.params.id);
    res.status(200).json({
      status: 'success',
      data: {
        tour
      }
    });

  } catch (e) {
    res.status(400).json({
      status: 'fail',
      message: e.message
    });
  }
};

exports.createTour = async(req, res) => {
  try {
    const newTour = await Tour.create(req.body);
    // console.log(newTour);

    res.status(201).json({
      status: 'Success',
      data: {
        tour: newTour
      }
    })

  }catch(e) {
    res.status(400).json({
      status: 'fail',
      message: e
    })
  }
};

exports.updateTour = async(req, res) => {
  console.log(req.body)
  try {
    const tour = await Tour.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true
    })
    res.status(200).json({
      status: 'Success',
      data: {
        tour
      }
    })

  }catch(e) {
    res.status(400).json({
      status: 'fail',
      message: e.message
    })
  }
};

exports.deleteTour = async(req, res) => {
  try {
    const deleteTour = await Tour.findByIdAndDelete(req.params.id);
    res.status(204).json({
      status: 'success',
      message: null
    })
  } catch (error) {
    
  }
  res.status(204).json({
    status: 'success',
    data: null
  });
};
