const dotenv = require('dotenv');
const app = require('./app');
const mongoose = require("mongoose");

dotenv.config({ path: './config.env' });

const DB = process.env.DATABASE;
mongoose.connect(DB).then((con) => console.log(con.connection + 'DB connection successful!')).catch(err => console.log('something went wrong', err))

const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`App running on port ${port}...`);
});
